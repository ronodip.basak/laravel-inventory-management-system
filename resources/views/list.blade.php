@extends('layouts.app')

@section('content')
    <table class="table">
        <thead>
        <tr>
            <th scope="col">ID</th>
            <th scope="col">Device Type</th>
            <th scope="col">Make</th>
            <th scope="col">Model</th>
            <th scope="col">Serial No.</th>
            <th scope="col">Spec</th>
            <th scope="col">Condition</th>
            <th scope="col">SUK</th>
            <th scope="col">Purchase Date</th>
            <th scope="col">Invoice</th>
            <th scope="col">Location</th>
            <th scope="col">Assigned To</th>
            <th scope="col">Created at</th>
            <th scope="col">Updated at</th>

        </tr>
        </thead>
       <tbody>
       @foreach($stocks as $stock)
           <tr>
               <td>{{ $stock->id }}</td>
               <td>{{ $device_type->where('id', $stock->device_type)->pluck('device_type')->first() }}</td>
               <td>{{ $stock->make }}</td>
               <td>{{ $stock->model }}</td>
               <td>{{ $stock->slno }}</td>
               <td>{{ $stock->spec }}</td>
               <td>{{ $condition->where('id', $stock->condition)->pluck('condition')->first() }}</td>
               <td>{{ $stock->suk }}</td>
               <td>{{ $stock->purchase_date }}</td>
               <td>{{ $stock->inv }}</td>
               <td>{{ $location->where('id', $stock->location)->pluck('location')->first() }}</td>
               <td>{{ $assigned_to->where('id', $stock->assigned_to)->pluck('name')->first() }}</td>
               <td>{{ date('d-M-Y', strtotime($stock->created_at)) }}</td>
               <td>{{ date('d-M-Y', strtotime($stock->updated_at)) }}</td>
           </tr>

        @endforeach
        </tbody>
    </table>

@endsection
