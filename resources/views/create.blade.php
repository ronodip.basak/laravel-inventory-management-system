@extends('layouts.app')

@section('js')
    <script src="https://cdnjs.cloudflare.com/ajax/libs/dropzone/5.7.0/min/dropzone.min.js"></script>
@endsection

@section('content')

    <div class="container-sm">
        <h2>Add Stock</h2>
        <p>Use this form to add a new product or purchase</p>
        <form method="post" class="form" action="/create"  enctype="multipart/form-data">
            @csrf
            <div class="form-group">
                <label for="deviceType">Device Type:</label>
                <select class="form-control" id="deviceType" name="deviceType">
                    <option value=""></option>
                    @foreach($device_types as $device_type)
                        <option value="{{ $device_type->id }}">{{ $device_type->device_type }}</option>
                    @endforeach
                </select>
            </div>

            <div class="form-group">
                <label for="make">Make:</label>
                <input type="text" class="form-control" id="make" name="make">
            </div>

            <div class="form-group">
                <label for="model">Model:</label>
                <input type="text" class="form-control" id="model" name="model">
            </div>

            <div class="form-group">
                <label for="slno">Serial No.:</label>
                <input type="text" class="form-control" id="slno" name="slno">
            </div>

            <div class="form-group">
                <label for="spec">Specification:</label>
                <input type="text" class="form-control" id="spec" name="spec">
            </div>

            <div class="form-group">
                <label for="condition">Condition:</label>
                <select class="form-control" id="condition" name="condition">
                    <option value=""></option>
                    @foreach($conditions as $condition)
                        <option value="{{ $condition->id }}">{{ $condition->condition }}</option>
                    @endforeach
                </select>
            </div>

            <div class="form-group">
                <label for="suk">SUK Number:</label>
                <input type="suk" class="form-control" id="suk" name="suk">
            </div>

            <div class="form-group">
                <label for="purchaseDate">Purchase Date:</label>
                <input type="date" class="form-control" id="purchaseDate" name="purchaseDate">
            </div>


            <div class="form-group">
                <label for="inv">Upload Invoice:</label>
                <input type="file" class="form-control" id="inv" name="inv" multiple>
            </div>

            <div class="form-group">
                <label for="location">Location:</label>
                <select class="form-control" id="location" name="location">
                    <option value=""></option>
                    @foreach($locations as $location)
                        <option value="{{ $location->id }}">{{ $location->location }}</option>
                    @endforeach
                </select>
            </div>

            <div class="form-group">
                <label for="assignedTo">Assigned To:</label>
                <select class="form-control" id="assignedTo" name="assignedTo">
                    <option value=""></option>
                    @foreach($assigned_tos as $assigned_to)
                        <option value="{{ $assigned_to->id }}">{{ $assigned_to->name }}</option>
                    @endforeach
                </select>
            </div>

            <button type="submit" class="btn btn-primary">Submit</button>
        </form>
    </div>
@endsection
