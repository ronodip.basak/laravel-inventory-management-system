<?php

namespace App\Http\Controllers;

use App\Stock;
use Illuminate\Http\Request;

class StockController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index()
    {
        $stocks = \App\Stock::latest()->get();
        $device_type = \App\DeviceType::all();
        $condition = \App\Condition::all();
        $location = \App\Location::all();
        $assigned_to = \App\AssignedTo::all();


        return view('list', [
            'stocks' => $stocks,
            'device_type' => $device_type,
            'condition' => $condition,
            'location' => $location,
            'assigned_to' => $assigned_to
        ]);

        //return view('list', [
        //   'stocks' => $stocks,
        //    'device_type' => $device_type
        //]);




    }

    public function create()
    {
        $device_type = \App\DeviceType::orderBy('device_type')->get();
        $condition = \App\Condition::orderBy('condition')->get();
        $location = \App\Location::orderBy('location')->get();
        $assigned_to = \App\AssignedTo::orderBy('name')->get();
        return view('create', [
        'device_types' => $device_type,
        'conditions' => $condition,
        'locations' => $location,
        'assigned_tos' => $assigned_to]
        );
        // return $device_type;
        // return $condition;
        // return $location;
        // return $assigned_to;

    }
    public function store()
    {
        Stock::create([
            'device_type' => request('deviceType'),
            'make' => \request('make'),
            'model' => \request('model'),
            'spec' => \request('spec'),
            'condition' => \request('condition'),
            'suk' => \request('suk'),
            'purchase_date' => \request('purchaseDate'),
            'inv' => \request('inv'),
            'location' => \request('location'),
            'assigned_to' => \request('assignedTo')
        ]);

    }
}
