<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Location extends Model
{
    //specify table name
    protected $table = 'location';
}
