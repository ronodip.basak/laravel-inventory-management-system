<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class AssignedTo extends Model
{
    //specify table name
    protected $table = 'assigned_to';
}
