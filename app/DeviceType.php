<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class DeviceType extends Model
{
    //specify table name
    protected $table = 'device_type';

}
