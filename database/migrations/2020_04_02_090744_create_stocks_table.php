<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateStocksTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('stocks', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('device_type');
            $table->string('make');
            $table->string('model');
            $table->string('slno')->unique()->nullable();
            $table->string('spec')->nullable();
            $table->unsignedBigInteger('condition');
            $table->string('suk')->nullable();
            $table->date('purchase_date')->nullable();
            $table->string('inv')->nullable();
            $table->unsignedBigInteger('location');
            $table->unsignedBigInteger('assigned_to');
            $table->timestamps();

            $table->foreign('device_type')
                ->references('id')
                ->on('device_type');


            $table->foreign('condition')
                ->references('id')
                ->on('condition');

            $table->foreign('location')
                ->references('id')
                ->on('location');

            $table->foreign('assigned_to')
                ->references('id')
                ->on('assigned_to');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('stocks');
    }
}
